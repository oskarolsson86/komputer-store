
let bankBalance = document.getElementById("bankBalance");
let pay = document.getElementById("pay");
let computerPrice = document.getElementById("computerPrice");
let salary = 100;

const commodor = new Computers("Commodore 64", 1500, "Old cool computer, can do stuff", "https://divadonna.files.wordpress.com/2007/03/tandy-trs80.jpg");
const macbook = new Computers("Macbook Pro", 15000, "New Computer - kind of", "https://icdn3.digitaltrends.com/image/cheap-macbook-touch-bar-720x720.jpg");
const thinkpad = new Computers("Thinkpad", 4000, "Boring computer - buy at your own risk", "https://icdn3.digitaltrends.com/image/cheap-macbook-touch-bar-720x720.jpg");
const dell = new Computers("Dell", 100, "I had one of these 15 years ago, pretty cool", "https://www.91-img.com/pictures/laptops/dell/dell-15-3551-x560139in9-pentium-quad-core-4-gb-500-gb-dos-75327-large-1.jpg?tr=q-60");


// Two checks, if the user already has a loan and that the user can only take a loan twice their current bank balance.
// Stored in localstorage if Sparbanken acceptes the loan.
const getLoan = () => {
    if (localStorage.getItem('gotLoan') == 'yes') {
        alert("You must buy a computer before you take another loan, mate..")
        return;
    }
    
    let stringLoan = prompt("Type amount to loan");
    
    let currentBalance = bankBalance.value;

    let loan = parseInt(stringLoan);
    let intBalance = parseInt(currentBalance);

    if (loan > currentBalance * 2) {
        alert("You can only loan twice your bank account amount")
    } else {
        bankBalance.value = loan + intBalance;
        localStorage.setItem('gotLoan', 'yes');
    }
    
}

// just depositing the pay balance to the bank account.
const addToBank = () => {
    let payVal = parseInt(pay.value);
    let intBal = parseInt(bankBalance.value);
    bankBalance.value = intBal + payVal;
    pay.value = 0;
}

// Adds a 100 kr to the pay balance for every click
const work = () => {
    let payVal = parseInt(pay.value);
    pay.value = salary + payVal;
}

// Checks if the user can afford the computer
const buy = () => {
    let intBal = parseInt(bankBalance.value);
    let intPrice = parseInt(computerPrice.value);

    if (intBal < intPrice) {
        alert("You gotta work harder and earn more money")
    } else {
        localStorage.clear();
        bankBalance.value = intBal - intPrice;
       alert("Congrats, you bought a 'great' computer.....")
    }
}

// Renders the different kind of computers from the dropdown list by a value(type). And displays the hidden DIV.
const renderComputerChoice = (type) => {
    
    let computerName = document.getElementById("computerName");
    let computerDescription = document.getElementById("description");
    let imageLink = document.getElementById("imageLink");

    switch (type) {
        case "commodor":
            $('#show').text(commodor.getName())
            computerName.innerHTML = commodor.getName()
            computerDescription.innerHTML = commodor.getDescription()
            computerPrice.value = commodor.getPrice()
            imageLink.src = commodor.getImgLink()
            break;
        case "macbook":
            $('#show').text(macbook.getName())
            computerName.innerHTML = macbook.getName()
            computerDescription.innerHTML = macbook.getDescription()
            computerPrice.value = macbook.getPrice()
            imageLink.src = macbook.getImgLink()
            break;
        case "thinkpad":
            $('#show').text(thinkpad.getName())
            computerName.innerHTML = thinkpad.getName()
            computerDescription.innerHTML = thinkpad.getDescription()
            computerPrice.value = thinkpad.getPrice()
            imageLink.src = thinkpad.getImgLink()
            break;
        case "dell":
            $('#show').text(dell.getName())
            computerName.innerHTML = dell.getName()
            computerDescription.innerHTML = dell.getDescription()
            computerPrice.value = dell.getPrice()
            imageLink.src = dell.getImgLink()
            break;
        default:
            break;
    }

    document.getElementById("hiddenPurchaseDiv").style.display = "flex"
  
}