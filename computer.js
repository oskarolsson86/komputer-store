class Computers {
    constructor(name, price, description, img) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.img = img;
    }
    getName() {
        return this.name;
    }

    getPrice() {
        return this.price;
    }

    getDescription() {
        return this.description;
    }

    getImgLink() {
        return this.img;
    }
}
